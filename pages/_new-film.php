<?php if(isset($error)):?>
    <?php if(!empty($error)):?>
        <blockquote>
            <p>При добавлении возникли следующие ошибки</p>
            <ol class="film_error">
                <?php foreach($error as $err):?>
                    <ol><?php echo $err;?></ol>
                <?php endforeach;?>
            </ol>
        </blockquote>
    <?php else:?>
        <p style="color: green">Успешно добавлено</p>
    <?php endif;?>
<?php endif;?>

<form class="form-horizontal" role="form" method="post">
    <div class="form-group">
        <label for="inputName" class="col-sm-2 control-label">Название</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="inputName" name="name" placeholder="Брюс Всемогущий" required="required">
        </div>
    </div>
    <div class="form-group">
        <label for="inputPassword3" class="col-sm-2 control-label">Год</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="inputYear" name="year" placeholder="2015" pattern="[0-9]{4}" required="required">
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="isActive" value="1"> Публиковать?
                </label>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-default">Отправить</button>
        </div>
    </div>
</form>