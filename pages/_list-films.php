<table class="table">
    <thead>
        <tr>
            <th>#</th>
            <th>Название</th>
            <th>Год</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($films as $key => $value): ?>
            <tr class="list-films">
                <td>
                    <p><?php echo ++$key;?></p>
                </td>
                <td>
                    <p><?php echo $value['name'] ?></p>
                </td>
                <td>
                    <p class="year"><?php echo $value['year'] ?></p>
                </td>
            </tr>
        <?php endforeach;?>
    </tbody>
</table>