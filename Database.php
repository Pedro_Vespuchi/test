<?php
/**
 * Created by PhpStorm.
 * User: Иванов
 * Date: 16.10.15
 * Time: 16:36
 */

class Database {
    public $dbc;
    private static $instance;

    private function __construct() {
        $config = require_once('config.php');
        $this->dbc = new PDO('mysql:host='.$config['host'].';dbname='.$config['dbname'], $config['user'] , $config['pass'], $config['options']);
    }
    public static function getInstance()
    {
        if (!isset(self::$instance))
        {
            $object = __CLASS__;
            self::$instance = new $object;
        }
        return self::$instance;
    }
}

class Item {
    public $db;
    public function __construct(){
        $this->db = Database::getInstance();
    }

    public function getAllItem($table){
        $sql = "select * from ".$table;   //prepared statements
        $query = $this->db->dbc->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }
    public function getItem($find, $table){
        $sql = "select * from ".$table. " WHERE ".key($find)."='".current($find)."'";   //prepared statements
        $query = $this->db->dbc->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }
    public function login($email, $pass, $table){
        $sql = "select * from ".$table. " WHERE email='".$email."' AND pass='".$pass."'";   //prepared statements
        $query = $this->db->dbc->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }
    public function addItem($item, $table){
        //insert operation
        $fields = array_keys($item);
        $values = array_values($item);
        $fieldlist = implode(',', $fields);
        $qs = str_repeat("?,", count($fields)-1);

        $sql = "INSERT INTO `".$table."` (".$fieldlist.") VALUES (${qs}?)";

        $query = $this->db->dbc->prepare($sql);
        $result = $query->execute($values);

        /*        if($result)
                    echo "Insert Successfull.";
                else
                    echo "Insert Fail.";*/
    }

    function updateArray($table, $id, $array) {
        $fields=array_keys($array);
        $values=array_values($array);
        $fieldlist=implode(',', $fields);
        $qs=str_repeat("?,",count($fields)-1);
        $firstfield = true;

        $sql = "UPDATE `".$table."` SET";
        for ($i = 0; $i < count($fields); $i++) {
            if(!$firstfield) {
                $sql .= ", ";
            }
            $sql .= " ".$fields[$i]."=?";
            $firstfield = false;
        }
        $sql .= " WHERE `id` =?";
        $sth = $this->db->dbc->prepare($sql);
        $values[] = $id;
        return $sth->execute($values);
    }
}