﻿# Host: localhost  (Version: 5.5.41-log)
# Date: 2015-10-16 18:42:33
# Generator: MySQL-Front 5.3  (Build 4.205)

/*!40101 SET NAMES utf8 */;

#
# Structure for table "film"
#

DROP TABLE IF EXISTS `film`;
CREATE TABLE `film` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `year` year(4) DEFAULT NULL,
  `isActive` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Data for table "film"
#

INSERT INTO `film` VALUES (1,'titanik',2000,1),(2,'interstellar',2014,1),(3,'просто фильм',2004,0),(4,'asdf',2005,1),(5,'Закрытый фильм',1999,0),(6,'Закрытый фильм 2',1998,0),(7,'Хороший фильм',2004,1),(8,'афыв',1999,0),(9,'asdf',1999,0),(10,'sdfdas',1999,0);
