<?php
/**
 * Created by PhpStorm.
 * User: Иванов
 * Date: 16.10.15
 * Time: 16:36
 */

return array(
    'user'    => 'root',
    'pass'    => '',
    'host'    => 'localhost',
    'dbname'  => 'film',
    'options' => array (
        PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"
    )
);