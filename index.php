<?php
/**
 * Created by PhpStorm.
 * User: Иванов
 * Date: 16.10.15
 * Time: 16:35
 */


/*********************************/

$home_dir = '/';

/*******************************/


require_once('Database.php');
if(!isset($_GET['page'])) {
    $page = 'main';
} else {
    $page = addslashes(strip_tags(trim($_GET['page'])));

    if($_GET['page'] == 'list-films') {
        $db = new Item();
        $films = $db->getItem(array('isActive' => true), 'film');
    }
}

if($_SERVER['REQUEST_METHOD'] == 'POST'){
    if($_REQUEST['page'] == 'new-film'){
        /* обработка ошибок и сохранение */
        $error = array();
        $succsess = true;
        if (filter_var((int)$_POST['year'], FILTER_VALIDATE_INT, array("options" => array("min_range" => 1800, "max_range" => date("Y")))) === false) {
            $error[] = 'не правильный год';
            $succsess = false;
        }

        if(empty($_POST['name'])){
            $error[] = 'заполните поле название';
            $succsess = false;
        }

        if($succsess){
            $db = new Item();
            $db->addItem($_POST, 'film');
        }
    }
}

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Film</title>

        <!-- Bootstrap -->
        <link href="bootstrap-3/css/bootstrap.min.css" rel="stylesheet">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <link href="assets/css/style.css" rel="stylesheet">
    </head>
    <body>
        <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="<?php echo $home_dir?>">Films</a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav">
                        <li><a href="<?php echo $home_dir?>index.php?page=list-films">Список фильмов</a></li>
                        <li><a href="<?php echo $home_dir?>index.php?page=new-film">Новый фильм</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="container">

            <div class="starter-template">
                <?php
                    require_once ('pages/_'.$page.'.php');
                ?>
            </div>

        </div>



        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="assets/js/jquery.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="bootstrap-3/js/bootstrap.min.js"></script>
    </body>
</html>